require('dotenv').config();
const BASE_URL = process.env.BASE_URL;
const IS_LOCAL = process.env.IS_LOCAL;
const webpack = require('webpack');
    CleanWebpackPlugin  = require('clean-webpack-plugin'),
    ExtractTextPlugin   = require('extract-text-webpack-plugin');

const extractPlugin = new ExtractTextPlugin({ filename: './assets/css/app.css' });


module.exports = {
    devtool: IS_LOCAL ? "source-map" : "",

    resolve: {
        extensions: [".jsx", ".js", ".json"]
    },

    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loader: "babel-loader",
                exclude: /(node_modules|build|public)/,

                options: {
                    plugins: ["transform-class-properties"],
                    presets: [
                        ['react'],
                        ['stage-0'],
                        ['env', { targets: { browsers: [ 'last 2 versions'] }, }]
                    ],
                }
            },


            // sass-loader with sourceMap activated
            {
                test: /\.(s?css)$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [ 'css-loader', 'postcss-loader', "sass-loader" ]
                })
            },

            // file-loader(for images)
            {
                test: /\.(jpg|png|gif|svg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            publicPath: BASE_URL,
                            outputPath: '/assets/images/',
                        }
                    }
                ]
            },

            // favicon
            {
                test: /\.(ico)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'favicon.ico',
                            publicPath: BASE_URL,
                            outputPath: '/',
                        }
                    }
                ]
            },

            // file-loader(for fonts)
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            publicPath: BASE_URL,
                            outputPath: '/assets/css/fonts/',
                        }
                    }
                ]
            }
        ]
    },

    plugins: [
        extractPlugin,
        new webpack.DefinePlugin({
            BASE_URL: JSON.stringify(BASE_URL),
            IS_LOCAL: JSON.stringify(IS_LOCAL),
            'process.env.NODE_ENV': JSON.stringify(IS_LOCAL ? 'development' : 'production')
        })
    ],

};