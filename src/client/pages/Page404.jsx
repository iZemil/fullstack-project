import * as React from 'react';
import { Route } from 'react-router-dom';
import { Link } from "react-router-dom";
import { Icon } from 'semantic-ui-react';


const Page404 = () => {
    return (
        <Route render={({ staticContext }) => {
            if (staticContext) {
                staticContext.status = 404;
            }

            return (
                <div className="page-404">
                    <div className="container">
                        <Link to='/'><Icon name="home" />Back to the homepage</Link>
                        <div className="page-404__text">Page Not Found</div>
                        <div className="page-404__text-error">404 error</div>
                    </div>
                </div>
            );
        }}/>
    );
};

export default {
    component: Page404
}