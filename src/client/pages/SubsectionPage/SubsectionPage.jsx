// Component to view page with Static Tasks (quiz (check/radio), question)
// and interface to interact with different types of tasks

import * as React from "react";
import {connect} from 'react-redux';
import {
    checkTask,
    changeUserValue,
    fetchTasksIfNeeded,
    testMarkdown,
    cancelTask,
    nextTask,
    finishSubsection,
    clearSubsection,
} from '../../actions/subsection';
import requireAuth from '../../containers/hocs/requireAuth';

import TasksForm from './TasksForm';
import MarkdownPreviewer from '../../components/MarkdownPreviewer';
import NoDataFetching from '../../components/NoDataFetching';
import DefaultLoader from '../../components/DefaultLoader';

import {
    Dimmer,
    Loader,
} from 'semantic-ui-react';


class SubsectionPage extends React.Component {

    componentDidMount() {
        const {
            fetchTasksIfNeeded,
            match
        } = this.props;

        fetchTasksIfNeeded(match.params.subsection_id);
    }

    handleClickCancelTasks = () => this.props.cancelTask();

    handleTaskChange = (value) => this.props.changeUserValue(value);

    handleSubmit = (e, taskNdx) => {
        e.preventDefault();

        const {
            subsection,
            checkTask,
            nextTask
        } = this.props;

        if(subsection.isCheckStage) {
            nextTask();
        } else {
            checkTask(taskNdx);
        }
    }


    render() {
        const {
            subsection,
            finishSubsection,
            clearSubsection,
            testMarkdown,
        } = this.props;

        return (
            subsection.isFetching
                ?
                subsection.status === 'success'
                    ?
                    subsection.type !== 'markdown'
                        ?
                        <TasksForm
                            subsection={subsection}
                            handleSubmit={this.handleSubmit}
                            handleClickCancelTasks={this.handleClickCancelTasks}
                            handleTaskChange={this.handleTaskChange}
                            finishSubsection={finishSubsection}
                            clearSubsection={clearSubsection}
                        />
                        :
                        <MarkdownPreviewer
                            subsection={subsection}
                            handleTestMarkdown={testMarkdown}
                            handleTaskChange={this.handleTaskChange}
                            handleClickCancelTasks={this.handleClickCancelTasks}
                        />
                    :
                    <NoDataFetching/>
                :
                <DefaultLoader />
        );
    }
}


const mapStateToProps = state => ({
    subsection: state.currentSubsection,

});

const mapDispatchToProps = dispatch => ({
    checkTask: (data) => dispatch(checkTask(data)),
    nextTask: () => dispatch(nextTask()),
    changeUserValue: (value) => dispatch(changeUserValue(value)),
    fetchTasksIfNeeded: (subsection_id) => dispatch(fetchTasksIfNeeded(subsection_id)),
    testMarkdown: () => dispatch(testMarkdown()),
    cancelTask: () => dispatch(cancelTask()),
    finishSubsection: () => dispatch(finishSubsection()),
    clearSubsection: () => dispatch(clearSubsection()),

});

export default {
    component: connect(mapStateToProps, mapDispatchToProps)(requireAuth(SubsectionPage)),

}
