import * as React from "react";
import classNames from 'classnames';

import {
    Button,
    Icon,
} from 'semantic-ui-react';


const BottomNav = ({ subsection }) => (
    <div className={ classNames('task-page__nav-bar', {
        "task-page__nav-bar_wrong": subsection.isCheckStage && subsection.lastAnswerIsWrong,
        "task-page__nav-bar_correct": subsection.isCheckStage && !subsection.lastAnswerIsWrong
    }) }>
        <div
            className={ classNames('task-page__nav-bar-container', {
                'task-page__nav-bar-container_check-stage': subsection.isCheckStage
            }) }>

            {
                subsection.isCheckStage
                &&
                (subsection.lastAnswerIsWrong
                        ?
                        <div className="task-page__nav-bar-isanswer">
                            <Icon color="red" size="huge" circular={true} name='close'/>Wrong
                        </div>
                        :
                        <div className="task-page__nav-bar-isanswer">
                            <Icon color="green" size="huge" circular={true} name='checkmark'/>Correct
                        </div>
                )
            }
            {
                subsection.isCheckStage
                    ?
                    <Button size="big" color={subsection.lastAnswerIsWrong ? 'red' : 'green'} type="submit" content="Next"/>
                    :
                    <Button primary size="big" type="submit" content="Check"/>
            }
        </div>
    </div>
);

export default BottomNav;