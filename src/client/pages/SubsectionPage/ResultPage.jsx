import * as React from "react";
import {Link} from 'react-router-dom';

import {
    Button,
} from 'semantic-ui-react';


export default class ResultPage extends React.Component {

    componentDidMount() {
        const {
            handleFinishSubsection,
        } = this.props;

        handleFinishSubsection();
    }

    componentWillUnmount() {
        this.props.handleClearSubsection();
    }

    render() {
        const {
            data
        } = this.props;

        const viewResult = wrong_answers => {
            switch(wrong_answers) {
                case 0:
                    return (
                        <div className="result-page__view-result-content">
                            <div className="result-page__view-result-title">Absolutely!</div>
                            <h2 className="result-page__smile">😃</h2>
                        </div>
                    );

                case 1:
                    return (
                        <div className="result-page__view-result-content">
                            <div className="result-page__view-result-title">Good</div>
                            <h2 className="result-page__smile">😜</h2>
                        </div>
                    );

                case 2:
                    return (
                        <div className="result-page__view-result-content">
                            <div className="result-page__view-result-title">Not bad...</div>
                            <h2 className="result-page__smile">🤔</h2>
                        </div>
                    );

                case 3:
                    return (
                        <div className="result-page__view-result-content">
                            <div className="result-page__view-result-title">You should repeat this</div>
                            <h2 className="result-page__smile">🤔</h2>
                        </div>
                    );

                default:
                    return (
                        <div className="result-page__view-result-content">
                            <div className="result-page__view-result-title">So sad...</div>
                            <h2 className="result-page__smile">😥</h2>
                        </div>
                    );
            }
        }


        return (
            <div className="result-page">
                <div className="container">
                    <div className="result-page__content">
                        <div className="result-page__progress">
                            <div className="result-page__view-result">
                                { viewResult(data.wrong_answers) }
                                <p className="result-page__text">Your result: {data.wrong_answers} mistakes</p>
                            </div>
                        </div>
                        <div className="result-page__sidebar">
                            <h3>Advertise</h3>
                            <div className="result-page__sidebar-a"></div>
                        </div>
                    </div>

                    <div className="result-page__bottom">
                        <div className="result-page__bottom-container">
                            <Button primary as={Link} to="/" size="big" content="Next"/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}