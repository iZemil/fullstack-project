import * as React from "react";
import { Link } from "react-router-dom";

import NoDataFetching from '../../components/NoDataFetching';
import Quiz from '../../components/StaticTasks/Quiz';
import Question from '../../components/StaticTasks/Question';
import BottomNav from './BottomNav';
import ResultPage from './ResultPage';

import {
    Progress,
    Form,
    Icon,
} from 'semantic-ui-react';


const TasksForm = ({
    subsection,
    handleSubmit,
    handleClickCancelTasks,
    finishSubsection,
    clearSubsection,
    handleTaskChange,
}) => {
    const tasks = subsection.tasks,
          tasksNumber = tasks.length,
          progress = subsection.progress,
          percent = 100 / tasksNumber * progress,
          success = progress >= tasksNumber;

    // First find task which has not ever passed
    let taskNdx = tasks.findIndex((it) => it.isCorrect === undefined);
    // ...then look through nonCorrect tasks
    if(!~taskNdx) {
        taskNdx = tasks.findIndex((it) => !it.isCorrect);
    }

    // view task
    const task = tasks[subsection.lastAnsweredNdx] || tasks[taskNdx];

    const renderTask = (task) => {

        switch(task.type) {
            case 'radioTest':
                return <Quiz
                            data={task}
                            currentValue={subsection.currentUserValue}
                            handleChange={ handleTaskChange }
                        />;

            case 'question':
                return <Question
                            data={task}
                            currentValue={subsection.currentUserValue}
                            handleChange={ handleTaskChange }
                        />;

            default:
                return null;
        }
    };

    const viewTasks = (progress < tasks.length) || subsection.isCheckStage;

    return (
        subsection.tasks
            ?
            viewTasks
                ?
                <div className="container">
                    <Form className="task-page" onSubmit={(e) => handleSubmit(e, taskNdx)}>
                        {/* TOP BAR */}
                        <div className="task-page__top-bar">
                            <Link className="task-page__top-bar-cancel" to='/'
                                  onClick={ () => handleClickCancelTasks() }
                                ><Icon color="grey" size="big" link name='window close outline'/>
                            </Link>

                            <Progress success={success} percent={percent} />

                            <div className="task-page__top-bar-keyboard">
                                <Icon color="grey" size="large" link name="keyboard" title="shortcut keys" />
                            </div>
                        </div>

                        {/* STATIC TASKS */}
                        <div className="task-page__content">
                            {renderTask(task)}
                        </div>

                        {/* BOTTOM ROW NAVIGATION */}
                        <BottomNav subsection={subsection} />
                    </Form>
                </div>
                :
                <ResultPage
                    data={subsection}
                    handleFinishSubsection={finishSubsection}
                    handleClearSubsection={clearSubsection}
                />
            :
            <NoDataFetching />
    );
};

export default TasksForm;