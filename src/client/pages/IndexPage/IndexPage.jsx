import * as React from "react";
import { connect } from 'react-redux';
import requireAuth from '../../containers/hocs/requireAuth';

import { Dimmer, Loader, Icon } from 'semantic-ui-react';
import SectionCard from './SectionCard';
import SideData from '../../containers/SideData';
import NoDataFetching from '../../components/NoDataFetching';
import DefaultLoader from '../../components/DefaultLoader';
import {fetchMapIfNeeded} from '../../actions/map';

import './index-page.scss';


class IndexPage extends React.Component {
    componentDidMount() {

        this.props.fetchMapIfNeeded();
    }

    render() {
        const {
            map,
            user
        } = this.props;

        const userMapSkills = user.skills ? user.skills[map._id] : null;

        return (
            <main className="container">
                <SideData />

                <div className="content_sidebar">
                    {
                        map.isFetching
                            ?
                            !!map.sections.length
                                ?
                                <div className="index-page">
                                    <h1 className="index-page__title"><Icon name="code" />{ map.title }</h1>
                                    <div className="index-page__section-cards">
                                        { map.sections.map((section, ndx) => <SectionCard {...section} skills={userMapSkills} key={ndx} />) }
                                    </div>
                                </div>
                                :
                                <NoDataFetching />
                            :
                            <DefaultLoader />
                    }
                </div>
            </main>
        );
    }
}


const mapStateToProps = state => ({
    map: state.map,
    user: state.user,
});

export default {
    component: connect(mapStateToProps, { fetchMapIfNeeded })(requireAuth(IndexPage)),
    loadData: ({dispatch}) => dispatch(fetchMapIfNeeded())
}