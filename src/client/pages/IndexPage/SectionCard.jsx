import * as React from "react";
import { Link } from "react-router-dom";

import cn from 'classnames';

import {
    Icon,
} from 'semantic-ui-react';


const SectionCard = ({ _id, title, description, subsections, skills, comingSoon }) => {
    const completedSubsections = skills ? (skills[_id] ? skills[_id].completed.length : 0) : 0;

    return (
        <Link
            className={cn("section__card", {"section__card_coming-soon": comingSoon})}
            to={`/section/${_id}`}
        >
            <div className="section__card-content">
                <h2 className="section__card-title">{title}</h2>

                {/*<div className="section__card-description">{description}</div>*/}

                <div className="section__card-progress">
                    { subsections.length > 1 && `completed: ${completedSubsections}/${subsections.length}` }
                </div>

                <div className="section__card-go">Go <Icon size="small" name='sign in' /></div>
            </div>
            <div className="overlay"></div>
        </Link>
    )
};

export default SectionCard;