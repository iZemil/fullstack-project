import * as React from "react";
import { Redirect, Link } from 'react-router-dom';
import {connect} from 'react-redux';
import {signUp, tryLogIn} from "../../actions/auth";

import LoginForm from '../../components/LoginForm';
import SignupForm from '../../components/SignupForm';
import {
    Button,
    Divider,
} from 'semantic-ui-react';

import getUrlParam from '../../helpers/getUrlParam';


class Login extends React.Component {
    state = {
        email: '',
        password: '',
        viewLoginForm: true,
    }

    componentDidMount() {
        const verificationEmail = getUrlParam('email');

        if(verificationEmail) {
            this.setState({ email: verificationEmail })
        }
    }

    toggleForm = () => {

        this.setState({
            viewLoginForm: !this.state.viewLoginForm,
            email: '',
            password: '',
        });
    }

    handleChange = (e) => {
        const t = e.target,
              value = t.value,
              name = t.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmitSignUp = (e) => {
        e.preventDefault();

        const {
            email,
            password,
        } = this.state;
        
        if(!email.trim() || !password.trim()) return false;

        const data = {
            email,
            password
        };

        // TODO: add redirect router to '/'
        this.props.signUp(data);
    }

    handleSubmitLogin = (e) => {
        e.preventDefault();

        const {
            email,
            password,
        } = this.state;

        if(!email.trim() || !password.trim()) return false;

        const data = {
            email,
            password
        };

        this.props.tryLogIn(data);
    }

    render() {
        const { from } = { from: { pathname: '/' } };
        const { user, auth } = this.props;
        const ui = user.ui;

        if (auth) {
            return <Redirect to={from} />
        }

        const {
            viewLoginForm,
            password,
            email,
        } = this.state;

        return (
            <section className="login-section">
                <div className="login-page">
                    <h2 className="login-page__title">{ viewLoginForm ? 'Log in' : 'Sign up' }</h2>
                    <div className="login-page__b-signup">
                        <div className="login-page__socials-auth">
                            <a href={`${BASE_URL}/auth/google`}>
                                <Button circular color='google plus' icon='google plus' />
                            </a>
                            <a href={`${BASE_URL}/auth/github`}>
                                <Button circular color='black' icon='github' />
                            </a>
                            {/*<a href={`${BASE_URL}/auth/fb`}>*/}
                                {/*<Button circular color='facebook' icon='facebook' />*/}
                            {/*</a>*/}
                            {/*<a href={`${BASE_URL}/auth/vk`}>*/}
                                {/*<Button circular color='vk' icon='vk' />*/}
                            {/*</a>*/}
                        </div>

                        <Divider horizontal>or</Divider>

                        {
                            viewLoginForm
                                ?
                                // Login form
                                <LoginForm
                                    ui={ui.login}
                                    handleSubmitLogin={this.handleSubmitLogin}
                                    email={email}
                                    password={password}
                                    toggleForm={this.toggleForm}
                                    handleChange={this.handleChange}
                                />
                                :
                                // Sign up form
                                <SignupForm
                                    ui={ui.signup}
                                    handleSubmitSignUp={this.handleSubmitSignUp}
                                    email={email}
                                    password={password}
                                    toggleForm={this.toggleForm}
                                    handleChange={this.handleChange}
                                />
                        }
                    </div>
                </div>
            </section>
        );
    }
}


const mapStateToProps = ({user, auth}) => ({ user, auth });

export default {
    component: connect(mapStateToProps, { signUp, tryLogIn })(Login),

}