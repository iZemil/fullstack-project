import * as React from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';

import DefaultLoader from '../../components/DefaultLoader';


export default (ChildComponent) => {
    class RequireAuth extends React.Component {

        render() {
            switch(this.props.auth) {
                case false:
                    return <Redirect to="/login" />;
                case null:
                    return <DefaultLoader />;
                default:
                    return <ChildComponent {...this.props} />;
            }
        }
    }

    function mapStateToProps({ auth }) {
        return { auth }
    }

    return connect(mapStateToProps)(RequireAuth);
};