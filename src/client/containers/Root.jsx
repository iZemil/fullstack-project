import * as React from "react";
import { connect } from 'react-redux';
import { renderRoutes } from 'react-router-config';

import Header from '../components/Header';
import {fetchCurrentUser} from "../actions/user";
import {logout} from "../actions/auth";


class Root extends React.Component {
    componentDidMount() {
        this.props.fetchCurrentUser();
    }

    handleLogout = () => {

        this.props.logout();
    }


    render() {
        const {
            route,
            user,
            auth
        } = this.props;


        return (
            <div className="app">
                <Header {...user} auth={auth} logout={this.handleLogout} />

                {/* MAIN CONTENT */}
                { renderRoutes(route.routes) }
            </div>
        )
    }
};


const mapStateToProps = ({ user, auth }) => ({ user, auth });

export default {
    component: connect(mapStateToProps, { fetchCurrentUser, logout, })(Root),
    loadData: ({dispatch}) => dispatch(fetchCurrentUser()),

}