import * as React from "react";
import {
    Link,
    withRouter
} from "react-router-dom";
import cn from 'classnames';

import {
    Button,
    Menu,
    Dropdown,
    Image
} from 'semantic-ui-react';

const logo = require('../assets/media/logo.png');


const Header = ({ logout, name, history, location, auth, role }) => {

    function handleLogout(history) {
        logout();
        history.push('/login');
    }

    const hideHeader = ~location.pathname.indexOf(/subsection/) || false;

    return (
        <div className={ cn('page-header', {'page-header_hidden': hideHeader}) }>
            <div className="container">
                <Menu stackable inverted>
                    <Menu.Item as={Link} to='/' className="page-header__logo">
                        <img src={logo} alt="logo" className="page-header__logo-img" />Front<span className="text_red">Go</span>
                    </Menu.Item>

                    { auth && !!~role.indexOf('admin') && <Menu.Menu>
                        <Dropdown item trigger={<span>Test menu</span>}>
                            <Dropdown.Menu>
                                <Dropdown.Item as={Link} to='/admin'>Admin</Dropdown.Item>
                                <Dropdown.Item as={Link} to='/login'>Log in</Dropdown.Item>
                                <Dropdown.Item onClick={() => handleLogout(history)}>Log out</Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </Menu.Menu>}
                    <Menu.Menu position='right'>
                        {
                            auth
                            ?
                            <Dropdown
                                item
                                trigger={
                                    <span className="page-header__menu-user-info">
                                        <Image src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAAAAADmVT4XAAADr0lEQVR42u3bMaukMBAAYH/6QAoLwSqVlVsIW9illAVLwTaFsJXYpUonQsBr7ni7762ajDOR42769+bbODHJZDdZsWGfnbpXVXWvVauNw/6bBPVXy1NJAa+RVb2NBjBNBh9CVNrFAJhawFbI3nED5mY7PQBAMfECRgkHIR6OEdAJOI7ScgGcAq+QhgfgavCM3LAAFHhHbhkAHQREsZADRhECAEUNWCSEhSYGNIH5IZ9JAVaEAqAhBdTB+SGdCQGIAQB4EAIaRH7IFzKAyzEAGMgAT1R+qMkADQ6QLVQAiQPASARAzQEAgI4IgCwBgDsRoMUCJBGgxgJgoQGUaIChAUg04EkDSNEATQJwAg3oSQDLXwy4/BHoq4twoAHkV0/DAg2YaAB3NMDSABo0wNEAemz+nGg1HLGAiggwY+dhS7UnLPhmoR9A4fKLmQqgcYCS7FwwC7YS8Dwb4jZlhg7QYfJLRwdAHU28noBvg+LG9QR8AZppDngDloxjNxTSJXsEL0SOFmBTlhIM6JQGvo6zmRoQOBN9ByCgWa1YBiAAEFQF/UoPCHkfFzw3Jt77EjGxANbJtw6blQfg+zbyvy4JBfg9BBF0dxl2b2gz0hkQDliH4zJQKyfgeC6Wjhdw9ELM55UZcLAzGFZuwH7jNl/5AUacvaM4CXAZxSp8ArDbudVXA4YIgCW9eASms5c0ZwEKrp0FJiW4MD4BWAq6vQgGYA53BB3nYjTVHpsy2c08gLn33ZSm98GRAyYVdDjMG0MJcBrRKSw1VYtm6ZD3dnlL0S2f2wzQkSp7ErCcSQ8AIGpzArB0J9N7EPYAWgJJiL0HsQ0wNyCLdPv9uAVYGgGUIccwwCiBOISa/QGO+OPvDUKCWvKQg9A6L4BOgSuq2QPQCuALaY8ATgFr/PjC7TeAq4E5smkXwJ4fIDM7gAdEiPc6SAiu54K3Km4DYLM4gLfzUxK3AH6vTfYjwIpYgNfjSxK5An+c4JLzd+Tn7tW/AC6NCOg+AGzE/C+9pC+AuRow/Qf88zVw+Sy4/D0Qby16W40SzzZ4jMVoVbHyv/72IgnoAZIdUMatLZmVUfLr7U2pjTAG6bB7Lmi4K7EwB0ezqWT9+D/Opx8OpwMb4VPTLPncFuXYoBcfm8gbHZJlqEkNoniY0CaVm9obzfKQ1zs/C99vVDrTq/LEUAh5bwd7tlfs7Ng3dZGn/jNUZPKmWm2IfuTypy6sGQfdtY1SdVWV3+JW3WvVPLpePyc7+98X/AJjhWTHMeBLbQAAAABJRU5ErkJggg==' avatar />
                                        { name ? name : 'User' }
                                    </span>
                                }
                            >
                                <Dropdown.Menu>
                                    <Dropdown.Item as={Link} to='/profile'>Profile</Dropdown.Item>
                                    <Dropdown.Item onClick={() => handleLogout(history)}>Log out</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                            :
                            location.pathname === '/login'
                                ?
                                null
                                :
                                <Menu.Item>
                                    <Button primary as={Link} to='/login'>Log in</Button>
                                </Menu.Item>
                        }
                    </Menu.Menu>
                </Menu>
            </div>
        </div>
    );
}

export default withRouter(Header)