import * as React from "react";
import moment from 'moment';
import cn from 'classnames';

import './statistics-calendar.scss';


export default class GitHubCalendar extends React.Component {
    constructor() {
        super();

        this.monthLabelHeight = 15;
        this.weekLabelWidth = 20;
        this.panelSize = 11;
        this.panelMargin = 2;

        this.state = {
            columns: 16,
            maxWidth: 40,
            hoverValue: null
        }
    }

    getPanelPosition(row, col) {
        var bounds = this.panelSize + this.panelMargin;
        return {
            x: this.weekLabelWidth + bounds * row,
            y: this.monthLabelHeight + bounds * col
        };
    }

    makeCalendarData(history, lastDay, columns) {
        var lastWeekend = new Date(lastDay);
        lastWeekend.setDate(lastWeekend.getDate() + (6 - lastWeekend.getDay()));

        var _endDate = moment(lastDay, 'YYYY-MM-DD');
        _endDate.add(1, 'days');
        _endDate.subtract(1, 'milliseconds');

        var result = [];
        for (var i = 0; i < columns; i++) {
            result[i] = [];
            for (var j = 0; j < 7; j++) {
                var date = new Date(lastWeekend);
                date.setDate(date.getDate() - ((columns - i - 1) * 7 + (6 - j)));

                var momentDate = moment(date);
                if (momentDate < _endDate) {
                    var key = momentDate.format('YYYY-MM-DD');
                    result[i][j] = {
                        value: history[key] || 0,
                        date: date
                    };
                } else {
                    result[i][j] = null;
                }
            }
        }

        return result;
    }

    handleHover = (e) => {
        const t = e.target;
        if(t.tagName !== 'rect') return;

        const title = t.getAttribute('title');

        this.setState({hoverValue: title});
    }

    render() {
        const {
            columns,
            hoverValue
        } = this.state;
        const {
            values,
            until,
        } = this.props;

        var contributions = this.makeCalendarData(values, until, columns);
        var innerDom = [];

        // panels
        for (var i = 0; i < columns; i++) {
            for (var j = 0; j < 7; j++) {
                var contribution = contributions[i][j];
                if (contribution === null) continue;
                const pos = this.getPanelPosition(i, j);
                const color = this.props.panelColors[contribution.value];
                const dom = (
                    <rect
                        key={ 'panel_key_' + i + '_' + j }
                        x={ pos.x}
                        y={ pos.y }
                        width={ this.panelSize }
                        height={ this.panelSize }
                        title={contribution.value}
                        fill={ color }
                    />
                );

                innerDom.push(dom);
            }
        }

        // week texts
        for (var i = 0; i < this.props.weekNames.length; i++) {
            const textBasePos = this.getPanelPosition(0, i);
            const dom = (
                <text
                    key={ 'week_key_' + i }
                    className='week'
                    x={ textBasePos.x - this.panelSize / 2 - 10 }
                    y={ textBasePos.y + this.panelSize / 2 + 4 }
                    textAnchor={ 'middle' }>
                    { this.props.weekNames[i] }
                </text>
            );
            innerDom.push(dom);
        }

        // month texts
        var prevMonth = -1;
        for (var i = 0; i < columns; i++) {
            if (contributions[i][0] === null) continue;
            var month = contributions[i][0].date.getMonth();
            if (month != prevMonth) {
                var textBasePos = this.getPanelPosition(i, 0);
                innerDom.push(<text
                        key={ 'month_key_' + i }
                        className='month'
                        x={ textBasePos.x + this.panelSize / 2 }
                        y={ textBasePos.y - this.panelSize / 2 - 2 }
                        textAnchor={ 'middle' }>
                        { this.props.monthNames[month] }
                    </text>
                );
            }
            prevMonth = month;
        }

        return (
            <div ref="calendarContainer" className="calendar-wrapper">
                <svg className="calendar" height="110" onMouseOver={(e) => this.handleHover(e)}>
                    {innerDom}
                </svg>
                <p className={cn("calendar__completed", {"calendar__completed_visible": +hoverValue > 0})}>completed: {hoverValue}</p>
            </div>
        );
    }

};

GitHubCalendar.defaultProps = {
    weekNames: ['', 'Mon', '', 'Wed', '', 'Fri', ''],
    monthNames: [
        'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
    ],
    panelColors: ['#EEE', '#AAA', '#888', '#444'],
};