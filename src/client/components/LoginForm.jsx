import * as React from "react";

import cn from 'classnames';
import {
    Form,
    Button,
} from 'semantic-ui-react';


export default ({ handleSubmitLogin, email, password, handleChange, toggleForm, ui }) => {

    return (
        <Form className="login-page__form" onSubmit={(e) => handleSubmitLogin(e)}>
            <Form.Field>
                <input type="text"
                       placeholder='E-mail'
                       autoComplete="true"
                       name="email"
                       value={email}
                       onChange={(e) => handleChange(e)}
                />
            </Form.Field>
            <Form.Field>
                <input type="password"
                       placeholder='Password'
                       autoComplete="true"
                       name="password"
                       value={password}
                       onChange={(e) => handleChange(e)}
                />
            </Form.Field>

            <p className={cn("login-page__message", {})}>{ ui.message }</p>

            <div className="login-page__form-bottom">
                <Button className="login-page__form-submit" type='submit' content="Log in" />
                <a className="login-page__toggle-form"
                   onClick={toggleForm}
                >No account? Sign Up</a>
            </div>
        </Form>
    );
}