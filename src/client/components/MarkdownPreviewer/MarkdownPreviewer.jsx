import * as React from "react";
import { Link } from "react-router-dom";

const marked = require('marked');

import {
    TextArea,
    Icon,
    Button,
} from 'semantic-ui-react';
import cn from 'classnames';

function CodeInput ({value, onCodeInputChange }) {
    return (
        <div className="markdown-input">
            <TextArea placeholder='Markdown...' value={value} onChange={onCodeInputChange } />
        </div>
    );
}

function CodeOutput({value}) {
    return (
        <div className="markdown-output" dangerouslySetInnerHTML={{__html: marked(value)}} />
    );
}

class MarkdownPreviewer extends React.Component {

    handleCodeInputChange = (e) => this.props.handleTaskChange(e.target.value);

    handleTest = () => this.props.handleTestMarkdown();

    render() {
        const {
            description,
            title,
            currentUserValue,
            isCorrectMarkdown
        } = this.props.subsection;

        return (
            <div className="container">
                <div className="markdown-page__top-bar">
                    <Link className="markdown-page__top-bar-cancel" to='/'
                          onClick={ () => this.props.handleClickCancelTasks() }
                    ><Icon color="grey" size="big" link name='window close outline'/>
                    </Link>
                    <h2>{ title }</h2>
                </div>
                <p dangerouslySetInnerHTML={{__html: description}} />
                <div className="mardown-previewer">
                    <CodeInput onCodeInputChange={this.handleCodeInputChange} value={currentUserValue} />
                    <CodeOutput value={currentUserValue} />
                </div>
                <div className={cn("markdown-page__bottom-row", {"markdown-page__bottom-row_error": isCorrectMarkdown === false})}>
                    { isCorrectMarkdown === false ? <div className="markdown-page__error-msg">Markdown is not correct!</div> : null }
                    {
                        isCorrectMarkdown
                            ?
                            <Button as={Link} to="/" className="markdown-page__check-btn" content="Correct" color="green" />
                            :
                            <Button className="markdown-page__check-btn" content="Test" onClick={this.handleTest} />
                    }
                </div>
            </div>
        );
    }
}


export default MarkdownPreviewer