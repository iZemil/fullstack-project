import * as React from "react";

import StaticsCalendar from './StatisticsCalendar';
import moment from 'moment';

import {
    Icon,
    Button,
    Modal
} from 'semantic-ui-react';


export default class Sidebar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLibraryModalOpen: false
        };

    }

    render() {
        const {
            isLibraryModalOpen,

        } = this.state;

        const values = {
            '2018-05-23': 1,
            '2018-05-26': 2,
            '2018-05-27': 3,
            '2018-05-28': 4,
            '2018-05-29': 4,
            '2018-05-20': 10
        };
        const until = moment().format('YYYY-MM-DD');


        return (
            <div className="sidebar">
                <div className="sidebar__block">
                    <h3 className="sidebar__block-title">Progress statistics</h3>
                    {/*<Icon className="sidebar__block-options" size="large" name='options' />*/}
                    <StaticsCalendar values={values} until={until} />
                </div>

                {/*<div className="sidebar__block">*/}
                    {/*<h3 className="sidebar__block-title">Progress statistics</h3>*/}
                    {/*/!*<Icon className="sidebar__block-options" size="large" name='options' />*!/*/}
                    {/*<div className="sidebar__chart"><StaticsCalendar /></div>*/}
                {/*</div>*/}

                {/*<div className="sidebar__block">*/}
                    {/*<h3 className="sidebar__block-title">Library</h3>*/}
                    {/*<Icon*/}
                        {/*className="sidebar__block-options"*/}
                        {/*size="large"*/}
                        {/*name='bookmark'*/}
                        {/*onClick={ () => this.handleOpenLibraryModal() }*/}
                    {/*/>*/}
                    {/*<p>Add links to the library</p>*/}
                {/*</div>*/}

                {/* MODAL FORM */}
                {/*<Modal*/}
                    {/*closeIcon*/}
                    {/*size="tiny"*/}
                    {/*open={isLibraryModalOpen}*/}
                    {/*onClose={this.handleCloseLibraryModal}*/}
                {/*>*/}
                    {/*<Modal.Header>Edit library links</Modal.Header>*/}
                    {/*<Modal.Content scrolling>*/}
                        {/*Editing interface for links*/}
                    {/*</Modal.Content>*/}

                    {/*<Modal.Actions>*/}
                        {/*<Button color="grey" content="Cancel" onClick={this.handleCloseLibraryModal} />*/}
                        {/*<Button color="blue" labelPosition='right' icon='checkmark' content='Save' />*/}
                    {/*</Modal.Actions>*/}
                {/*</Modal>*/}


                {/*<div className="sidebar__block">*/}
                {/*<h3 className="sidebar__block-title">Ежедневная цель</h3>*/}
                {/*<Icon className="sidebar__block-options" size="large" name='setting' />*/}
                {/*<div>График</div>*/}
                {/*</div>*/}
                {/*<div className="sidebar__block">*/}
                {/*<h3>Достижения</h3>*/}
                {/*<Icon className="sidebar__block-options" size="large" name='book' />*/}
                {/*</div>*/}
            </div>
        );
    }

    // handleOpenLibraryModal = () => {
    //     this.setState({ isLibraryModalOpen: true });
    // }
    //
    // handleCloseLibraryModal = () => {
    //     this.setState({ isLibraryModalOpen: false });
    // }
};