import * as React from 'react';

import { Dimmer, Loader, } from 'semantic-ui-react';


export default () => <Dimmer inverted active><Loader /></Dimmer>