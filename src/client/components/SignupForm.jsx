import * as React from "react";

import cn from 'classnames';
import { Form, Button, } from 'semantic-ui-react';


export default ({ handleSubmitSignUp, email, password, handleChange, toggleForm, ui }) => {

    return (
        <Form className="login-page__form" onSubmit={(e) => handleSubmitSignUp(e)}>
            <Form.Field>
                <input type="text"
                       placeholder='Write your e-mail'
                       name="email"
                       value={email}
                       onChange={(e) => handleChange(e)}
                />
            </Form.Field>
            <Form.Field>
                <input type="password"
                       placeholder='Create a password'
                       name="password"
                       value={password}
                       onChange={(e) => handleChange(e)}
                />
            </Form.Field>

            <p className={cn("login-page__message", {})}>{ ui.message }</p>

            <div className="login-page__form-bottom">
                <Button className="login-page__form-submit" color="black" type='submit' content="Sign up" />
                <a className="login-page__toggle-form"
                   onClick={toggleForm}
                >Already have an account? Log in</a>
            </div>
        </Form>
    );
}