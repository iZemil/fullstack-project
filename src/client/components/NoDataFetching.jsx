import * as React from "react";
import { Link } from "react-router-dom";

import { Icon } from 'semantic-ui-react';


export default function NoDataFetching() {

    return (
        <div>
            <Link to='/'><Icon name="home" />Back to the homepage</Link>
            <h2>No data 😥</h2>
        </div>
    )
}