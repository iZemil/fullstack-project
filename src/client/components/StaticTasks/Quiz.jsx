import * as React from "react";

import {
  Form,
  Checkbox
} from 'semantic-ui-react';


class Quiz extends React.Component {


    render() {
        const {
            handleChange,
            data,
            currentValue,
        } = this.props;

        return (
            <div className="quiz">
                <h2 className="quiz__title">{ data.title }</h2>
                <div className="quiz__list">
                {
                    data.options.map((answer, ndx) => {
                        const text = answer.text,
                              value = answer.id,
                              checked = (value === currentValue);

                        return (
                            <Form.Field className="quiz__item" key={ndx}>
                                <Checkbox
                                    radio
                                    label={text}
                                    name='quiz'
                                    value={value}
                                    checked={checked}
                                    onChange={ (e, {value}) => handleChange(value) }
                                />
                            </Form.Field>
                        );
                    })
                }
                </div>
            </div>
        );
    }
}

export default Quiz;