import * as React from "react";

import {
  TextArea
} from 'semantic-ui-react';


export default class Question extends React.Component {


    render() {
        const {
            data,
            handleChange,
            currentValue,
        } = this.props;

        return (
            <div className="b-code">
                <h2 className="b-code__title">{ data.title }</h2>
                {
                    data.description
                    &&
                    <p className="b-code__text">{ data.description }</p>
                }
                <TextArea
                    autoHeight
                    placeholder="Write your answer..."
                    value={ currentValue }
                    style={{ minHeight: 185 }}
                    onChange={ (e) => handleChange(e.target.value) }
                />
            </div>
        );
    }
}