import React from 'react';
import Root from '../containers/Root';
import IndexPage from '../pages/IndexPage/';
import SectionPage from '../pages/SectionPage';
import SubsectionPage from '../pages/SubsectionPage';
import AddingPage from '../pages/AddingDataForm';
import Profile from '../pages/Profile';
import LoginPage from '../pages/LoginPage';
import SectionOverview from '../pages/SectionPage/SectionOverview';
import SectionFinalProject from '../pages/SectionPage/SectionFinalProject';
import Page404 from '../pages/Page404';

import MarkdownPreviewerCSS from '../components/MarkdownPreviewerCSS';


export default [{
    ...Root,
    routes: [
        {
            path: '/',
            exact: true,
            ...IndexPage,
        },
        {
          path: '/markup',
          component: MarkdownPreviewerCSS
        },
        {
            path: '/login',
            ...LoginPage
        },
        {
            path: '/section/overview',
            ...SectionOverview
        },
        {
            path: '/section/final-project',
            ...SectionFinalProject
        },
        {
            path: '/section/:id',
            ...SectionPage,
        },
        {
            path: '/subsection/:subsection_id',
            ...SubsectionPage
        },
        {
            path: '/profile',
            ...Profile
        },
        {
            path: '/admin',
            ...AddingPage
        },
        {
            ...Page404
        },
    ]
}];