


export const REQUEST_MAP = 'request_map';
function requestMap() {
    return {
        type: REQUEST_MAP
    }
}

export const RECEIVE_MAP = 'receive_map';
function receiveMap(json) {
    return {
        type: RECEIVE_MAP,
        json
    }
}

export const ERROR_MAP = 'error_map';
const errorMap = (err) => ({ type: ERROR_MAP });


export function fetchMap() {
    const mapName = 'web';

    return (dispatch, getState, api) => {
        dispatch(requestMap());


        return api.get(`/map/${mapName}`)
            .then(res => res.data)
            .then(json => dispatch(receiveMap(json)))
            .catch(err => dispatch(errorMap(err)));
    }
}


function shouldFetchMap(state) {
    if(!state.map.isFetching) {
        return true
    }

    return false;
}

export function fetchMapIfNeeded() {
    return (dispatch, getState) => {
        if(shouldFetchMap(getState())) {
            return dispatch(fetchMap());
        }
    }
}