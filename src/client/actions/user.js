


export const FETCH_CURRENT_USER = 'fetch_current_user';
export function fetchCurrentUser() {

    return (dispatch, getState, api) => {

        return api.get('/current_user')
            .then((res) => res.data)
            .then(data => {
                dispatch({
                    type: FETCH_CURRENT_USER,
                    payload: data,
                })
                return data.result;
            })
            .then((data) => data ? dispatch(fetchUserData()) : null)
            .catch((err) => console.error('fetch_current_user error:', err));
    }
}

export const GET_USER_DATA = 'get_user_data';
export function fetchUserData() {

    return (dispatch, getState, api) => {

        return api.get('/user')
            .then(res => res.data)
            .then(json => {
                dispatch({
                    type: GET_USER_DATA,
                    data: json
                });
            })
            .catch(e => console.error(e));
    }
}

export const SEND_RESULT = 'send_result';
export const RESULTS_WERE_SENT = 'results_were_sent';
export function sendResult() {

    return (dispatch, getState, api) => {
        dispatch({ type: SEND_RESULT });

        const state = getState(),
            map = state.map,
            currentSection = state.currentSection,
            currentSubsection = state.currentSubsection;

        const map_id = map._id,
            section_id = currentSection._id,
            subsection_id = currentSubsection._id;

        api.post(`/user/skills`, {
            map_id,
            section_id,
            subsection_id,
            subsection: currentSubsection,
        })
            .then(() => dispatch({ type: RESULTS_WERE_SENT }))
            .catch(e => console.error(e));
    }
}

export const SAVE_PROFILE = 'save_profile';
export const RESET_PROFILE_MESSAGE = 'reset_profile_message';
export const saveProfile = (data) => {

    return (dispatch, getState, api) => {
        const {
            newPassword,
            oldPassword,
            name,
            email
        } = data;

        api.put('/user/profile', {
            newPassword,
            oldPassword,
            name,
            email
        })
            .then(res => res.data)
            .then(json => {
                dispatch({type: SAVE_PROFILE, json});
                setTimeout(() => dispatch({type: RESET_PROFILE_MESSAGE}), 2500);
            })
            .catch(err => console.error(err))
    }
};