import {sendResult} from "../actions/user";
import {fetchMap} from "./map";


export function fetchTasksIfNeeded(subsection_id) {
    return (dispatch) => {
        return dispatch(fetchTasks(subsection_id));
    }
}

export const REQUEST_TASKS = "request_task";
export const RECEIVE_TASKS = "receive_tasks";
function fetchTasks(subsection_id) {

    return (dispatch, getState, api) => {
        dispatch({ type: REQUEST_TASKS });

        return api.get(`/subsection/${subsection_id}`)
            .then(res => res.data)
            .then(json => dispatch(receiveTasks(json)))
            .catch(error => {
                dispatch({ type: RECEIVE_TASKS, status: "error" });
                console.error('fetchTasks error: ', error);
            });
    }
}

function receiveTasks(json) {
    return {
        type: RECEIVE_TASKS,
        status: 'success',
        json
    }
}

export const CHECK_STAGE = 'check_stage';
export const CORRECT_TASK_ANSWER = 'correct_task_answer';
export const INCORRECT_TASK_ANSWER = 'incorrect_task_answer';
export function checkTask(taskNdx) {

    return (dispatch, getState) => {
        const subsection = getState().currentSubsection,
            tasks = subsection.tasks,
            task = tasks[taskNdx],
            {answer, type} = task,
            {currentUserValue} = subsection;

        let isCorrectAnswer = false;

        if (type === 'radioTest') {
            isCorrectAnswer = (currentUserValue === answer);
        } else if (type === 'question') {
            isCorrectAnswer = new RegExp(answer).test(currentUserValue);
        }

        dispatch({ type: CHECK_STAGE, isCorrectAnswer, taskNdx });

        if (isCorrectAnswer) {
            return dispatch({ type: CORRECT_TASK_ANSWER, taskNdx });
        }

        return dispatch({ type: INCORRECT_TASK_ANSWER, taskNdx });
    }
}

export const NEXT_TASK = 'next_task';
export const nextTask = () => ({ type: NEXT_TASK });

export const CHANGE_USER_VALUE = 'change_user_value';
export const changeUserValue = (value) => ({ type: CHANGE_USER_VALUE, value });

export const TEST_MARKDOWN = 'test_markdown';
export const CORRECT_MARKDOWN = 'correct_markdown';
export const INCORRECT_MARKDOWN = 'incorrect_markdown';
export const testMarkdown = () => {

    return (dispatch, getState) => {
        dispatch({type: TEST_MARKDOWN});

        const subsection = getState().currentSubsection,
            {test, testNum, currentUserValue} = subsection;

        const regexValue = new RegExp(test, 'gi'),
            res = currentUserValue.match(regexValue);

        if(res && res.length === testNum) {
            dispatch(finishSubsection());
            return dispatch({type: CORRECT_MARKDOWN});
        }

        dispatch({type: INCORRECT_MARKDOWN});
    }
}

export const CANCEL_TASK = 'cancel_task';
export const cancelTask = () => ({ type: CANCEL_TASK });

export const FINISH_SUBSECTION = 'finish_subsection';
export function finishSubsection() {
    return (dispatch) => {
        dispatch({ type: FINISH_SUBSECTION });
        dispatch(sendResult());
        dispatch(fetchMap());
    }
}

export const CLEAR_SUBSECTION = 'clear_subsection';
export const clearSubsection = ()  => ({ type: CLEAR_SUBSECTION });