import 'babel-polyfill';
import * as React from "react";
import * as ReactDOM from "react-dom";

import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import routes from './routes';
import { createStore, applyMiddleware, compose } from 'redux';
import reducer from './reducers';
import thunk from 'redux-thunk';
import axios from 'axios';
import './assets/media/favicon.ico';
import './assets/styles/index.scss';


const axiosInstance = axios.create({
    // baseURL: '/api',
    baseURL: '/',
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const middleWares = IS_LOCAL ? composeEnhancers(applyMiddleware(thunk.withExtraArgument(axiosInstance))) : applyMiddleware(thunk.withExtraArgument(axiosInstance));

const store = createStore(
    reducer,
    window.INITIAL_STATE,
    middleWares,
);


ReactDOM.hydrate(
  <Provider store={store}>
      <BrowserRouter>
          <div>{ renderRoutes(routes) }</div>
      </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);