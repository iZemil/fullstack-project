import {
    REQUEST_TASKS,
    RECEIVE_TASKS,
    CHANGE_USER_VALUE,
    CHECK_STAGE,
    CORRECT_TASK_ANSWER,
    INCORRECT_TASK_ANSWER,
    NEXT_TASK,
    CORRECT_MARKDOWN,
    INCORRECT_MARKDOWN,
    CANCEL_TASK,
    CLEAR_SUBSECTION,

} from "../actions/subsection"


/**
 * 
 * REDUCERS
 */

const initialState_1 = {
    progress: 0,
    currentUserValue: '',
    isAnswered: false,
    isCheckStage: false,
    wrong_answers: 0,
    wrong_answers_indexes: [],
    lastAnswerIsWrong: false,
    tasks: []
}

export default function task(state = initialState_1, action) {
    switch (action.type) {
        case REQUEST_TASKS:
            return {
                ...state,
                isFetching: false
            };

        case RECEIVE_TASKS:
            return {
                ...state,
                status: action.status,
                isFetching: true,
                ...action.json
            };

        case CHANGE_USER_VALUE:
            return {
                ...state,
                currentUserValue: action.value,
                isAnswered: true,
            };

        case CHECK_STAGE:
            return {
                ...state,
                lastAnswerIsWrong: !action.isCorrectAnswer,
                isCheckStage: true,
                lastAnsweredNdx: action.taskNdx
            }

        case CORRECT_TASK_ANSWER:
            return {
                ...state,
                progress: state.progress + 1,
                tasks: state.tasks.map((task, ndx) => ndx === action.taskNdx ? {...task, isCorrect: true} : task)
            };

        case INCORRECT_TASK_ANSWER:
            return {
                ...state,
                wrong_answers: state.wrong_answers += 1,
                wrong_answers_indexes: [...state.wrong_answers_indexes, action.taskNdx],
                tasks: state.tasks.map((task, ndx) => ndx === action.taskNdx ? {...task, isCorrect: false} : task)
            };

        case NEXT_TASK:
            return {
                ...state,
                currentUserValue: '',
                isAnswered: false,
                isCheckStage: false,
                lastAnsweredNdx: null
            }

        case CORRECT_MARKDOWN:
            return {
                ...state,
                isCorrectMarkdown: true
            }

        case INCORRECT_MARKDOWN:
            return {
                ...state,
                isCorrectMarkdown: false
            }

        case CANCEL_TASK:
            return state = initialState_1;

        case CLEAR_SUBSECTION:
            return state = initialState_1;

        default:
            return state;
    }
}