import {
    LOGIN_SUCCESS,
    LOGIN_UNSUCCESS,
    SIGN_UP,
} from "../actions/auth";
import {
    GET_USER_DATA,
    SAVE_PROFILE,
    RESET_PROFILE_MESSAGE,
} from "../actions/user";


const initialState = {
    isAuthenticated: false,
    ui: {
        isAuthFetched: false,
        login: {
            message: ''
        },
        signup: {
            message: '',
            errors: null
        }
    },
}

export default function user(state = initialState, action) {
    switch (action.type) {
        case GET_USER_DATA:
            return {
                ...state,
                ...action.data
            }


        case LOGIN_SUCCESS:
        case LOGIN_UNSUCCESS:
            return {
                ...state,
                ui: {
                    ...state.ui,
                    login: {
                        message: action.json.message
                    }
                }
            }

        case SIGN_UP:
            return {
                ...state,
                ui: {
                    ...state.ui,
                    signup: {
                        ...state.ui.signup,
                        message: action.json.message
                    }
                }
            }

        case SAVE_PROFILE:
            return {
                ...state,
                name: state.name,
                profileMessage: action.json.message
            }

        case RESET_PROFILE_MESSAGE:
            return {
                ...state,
                profileMessage: ''
            }

        default:
            return state
    }
}