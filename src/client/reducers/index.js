import { combineReducers } from 'redux';
import auth from './auth';
import user from './user';
import map from './map';
import currentSection from './section';
import currentSubsection from './subsection';


const rootReducer = combineReducers({
    auth,
    user,
    map,
    currentSection,
    currentSubsection
});


export default rootReducer;