import {REQUEST_MAP, RECEIVE_MAP,ERROR_MAP} from "../actions/map";


export default function map(state = { sections: [] }, action) {
switch (action.type) {
    case REQUEST_MAP:
        return {
            sections: [],
            isFething: false,
        };

    case RECEIVE_MAP:
        return {
            isFetching: true,
            ...action.json
        };

    case ERROR_MAP:
        return {
            sections: [],
            isFetching: true
        };

    default:
        return state;
    }
}