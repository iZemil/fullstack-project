import passport from 'passport';
import options from './authOptions';


export default {
    google: (req, res, next) => {
        if(req.query.redirect) req.session.authRedirect = req.query.redirect;

        passport.authenticate('google', { scope : ['profile', 'email'] })(req, res, next);
    },
    googleCb: passport.authenticate('google', { failureRedirect: options.failureRedirect }),
    googleAfterCb: (req, res) => {
        const redirect = req.session.authRedirect;
        if(redirect) delete req.session.authRedirect;

        res.redirect(303, req.query.redirect || options.successRedirect);
    },

    github: passport.authenticate('github'),
    githubCb: passport.authenticate('github', { failureRedirect: '/login' }),
    githubAfterCb: (req, res) => {
        res.redirect('/');
    },

    facebook: (req, res, next) => {
        if (req.query.redirect) res.session.authRedirect = req.query.redirect;
        passport.authenticate('facebook')(req, res, next);
    },
    facebookCb: passport.authenticate('facebook', { failureRedirect: options.failureRedirect}),
    facebookAfterCb: (req, res) => {
        const redirect = req.session.authRedirect;
        if (redirect) delete req.session.authRedirect;
        res.redirect(303, redirect || options.successRedirect);
    },

}