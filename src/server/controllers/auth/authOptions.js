require('dotenv').config();

export default {
    failureRedirect: '/login',
    successRedirect: process.env.BASE_URL,

}