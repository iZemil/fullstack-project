require('dotenv').config();
import nodemailer from 'nodemailer';
import uuidv4 from 'uuid/v4';
import VerifyEmail from '../../models/verifyEmailsScheme';
import User from '../../models/userScheme';


const smtpTransport = nodemailer.createTransport({
    service: "Gmail",
    auth: {
        user: process.env.SMTP_EMAIL,
        pass: process.env.SMTP_PASS
    }
});
let rand,mailOptions,host,link;

const emailVerifyController = {
    sendData: (req, res) => {
        rand = uuidv4();
        host = req.get('host');
        link = `http://${req.get('host')}/email-verify?id=${rand}`;
        mailOptions = {
            from: '"FrontGo" <xxx@gmail.com>',
            to: req.body.email,
            subject: "Please confirm your Email account",
            html: `Hello, <br> Please Click on the link to verify your email.<br><a href=${link}>Click here to verify</a>`
        };

        // TODO: returned ip is ::1; need to make a defense to db don't overflow
        // const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

        User.findOne({ email: mailOptions.to }, function(err, user) {
            if(err) res.status(500).json({message: 'Email verify user error'});

            if(user) {
                res.status(200).json({message:'This user email is busy'});
            } else {
                smtpTransport.sendMail(mailOptions, function(error, response) {
                    if(error) {
                        res.status(500).json({message: 'Email verify user error'});
                    } else {

                        const verifyEmail = new VerifyEmail({
                            uuid: rand,
                            email: mailOptions.to,
                            password: req.body.password,
                        });

                        VerifyEmail.findOneAndUpdate({email: mailOptions.to}, {$set:{uuid: rand, password: req.body.password}}, function(err, vEmail) {
                            if(err) res.status(500).json({message: 'Email verify create error'});

                            if(vEmail) {
                                res.status(200).json({message: 'Check your email'});
                            } else {
                                verifyEmail.save(function(err, item) {
                                    if(err) res.status(500).json({message: 'Email verify create error'});

                                    if(item) {
                                        res.status(200).json({message: 'Check your email'});
                                    }
                                });
                            }
                        })
                    }
                });
            }
        });
    },

    verifyData: (req, res) => {
        if(req.query.id == rand) {
            // create verify user:
            VerifyEmail.findOne({email: mailOptions.to}, function (err, verifyEmail) {
                if(err) res.status(500).json({message: 'verify email error'});

                if(verifyEmail) {
                    const user = new User({
                        email: verifyEmail.email,
                        name: verifyEmail.email,
                        password: verifyEmail.password,
                    });

                    user.save(function(err) {
                        if (err) res.status(500).json({message: 'Create user error'});

                        res.redirect(`/login?email=${mailOptions.to}`);
                    });
                } else {
                    res.status(500).json({message: 'Create user: verify email error'});
                }
            });
        } else {
            res.end("<h1>Bad Request</h1>");
        }
    }
};


export default emailVerifyController;