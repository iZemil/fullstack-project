const Map = require('../models/mapScheme');


exports.map_get = function (req, res) {
    const map_id = req.params.map_id;

    Map.findById(map_id)
        .populate('sections')
        .exec(function (err, map) {
            if(err) handleError(err);

            res.status(200).json(map);
        });
};