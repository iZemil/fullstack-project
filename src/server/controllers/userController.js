import User from '../models/userScheme';


export function user_get(req, res) {
    const user_id = req.session.passport.user;

    User.findById(user_id, function(userErr, user) {
        if (userErr || !user) return res.status(401).end();

        res.status(200).json(user);
    });
}


export function user_post_skills (req, res) {

    const user_id = req.session.passport.user,
        body = req.body;
    const {
        map_id,
        section_id,
        subsection_id,
        subsection,
        wrongAnswers,
    } = body;
    const subsectionLink = `skills.${map_id}.${section_id}.${subsection_id}`,
        completedSubsectionLink = `skills.${map_id}.${section_id}.completed`;

    // TODO: add task validation?
    if(!(map_id && section_id && subsection)) return res.status(500).send('skills cannot be updated because undefined path to subsection');

    User.update({_id: user_id }, { $set: { [subsectionLink]: subsection }, $addToSet: { [completedSubsectionLink]: subsection_id } }, { upsert: true }, function(userErr, data) {
        if(userErr) return res.status(500).send('skills update error');

        return res.status(200).send('skills were updated successfully');
    });
}


export function user_put_profile(req, res) {
    const body = req.body;
    const {
        newPassword,
        oldPassword,
        name,
        email
    } = body;

    User.findOne({email}, function(err, user) {
        if(err) res.status(500).json({message: "Save profile error"});

        if(user) {
            // TODO: validation in scheme
            user.name = name;

            if(oldPassword) {
                return user.comparePassword(oldPassword, function(passwordErr, isMatch) {
                    if(!isMatch) res.status(200).json({message: 'Passwords don\'t match'});

                    user.password = newPassword;

                    user.save(function() {
                        res.status(200).json({message: 'Profile was changed successfully'});
                    });
                });
            }

            user.save(function() {
                res.status(200).json({message: 'Profile was changed successfully'});
            });
        }
    });
}