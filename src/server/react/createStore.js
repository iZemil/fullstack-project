require('dotenv').config();

import { createStore, applyMiddleware } from 'redux';
import reducers from '../../client/reducers/index.js';
import thunk from 'redux-thunk';
import axios from 'axios';


export default (req) => {
    const axiosInstance = axios.create({
        baseURL: process.env.BASE_URL,
        headers: { cookie: req.get('cookie') || '' }
    });

    const store = createStore(
        reducers,
        {},
        applyMiddleware(thunk.withExtraArgument(axiosInstance))
    );

    return store;
}