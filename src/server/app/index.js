require('dotenv').config();

// Common:
import express from 'express';
import mongoose from 'mongoose';
import morgan from 'morgan';
import session from 'express-session';
import mongoConnection from '../mongo';
import { authCheck } from "../middleware/authMiddleware";
import bodyParser from 'body-parser';
import cors from 'cors';

// Routes:
import mapRoutes from '../routes/mapRoute';
import sectionRoutes from '../routes/sectionRoute';
import subsectionRoutes from '../routes/subsectionRoute';
import userRoutes from '../routes/userRoute';

// React:
import { matchRoutes } from 'react-router-config';
import createStore from '../react/createStore';
import renderer from '../react/renderer';
import Routes from '../../client/routes';


const app = express();
const PORT = process.env.SERVER_PORT;
const isLocal = process.env.IS_LOCAL || false;
const auth = require('../lib/auth')(app);


// MongoDB connection:
mongoConnection.init(mongoose, isLocal);

// set morgan to log info about our requests for development use.
app.use(morgan('dev'));
app.use(express.static('public'));

// app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// initialize express-session to allow us track the logged-in user across sessions.
const MongoStore = require('connect-mongo')(session);
app.use(session({
    key: 'user_sid',
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: { }
}));

auth.initSmtp();
auth.init();
auth.registerRoutes();


app.get('/current_user', function(req, res) {
    res.status(200).json({"result": req.isAuthenticated()});
});
app.use('/map', authCheck, mapRoutes);
app.use('/user', authCheck, userRoutes);
// map route takes function to fetch section data
// app.use('/section', authCheck, sectionRoutes);
app.use('/subsection', authCheck, subsectionRoutes);

// server side rendered react
app.get('*', (req, res) => {
    const store = createStore(req);

    const promises = matchRoutes(Routes, req.path).map(({ route }) => {
        return route.loadData ? route.loadData(store) : null;
    }).map(promise => {
        if(promise) {
            return new Promise((resolve, reject) => {
                promise.then(resolve).catch(reject)
            })
        }
    });

    Promise.all(promises).then(() => {
        const context = {};
        const content = renderer(req, store, context);

        if(context.url) {
            return res.redirect(301, context.url);
        }
        if(context.notFound) {
            res.status(404);
        }

        res.send(content);
    });
});


app.listen(PORT, () => console.log('SERVER LISTENING ON PORT: ' + PORT));