const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const Section = require('./sectionScheme');


const mapScheme = new Schema({
  _id: String,
  title: String,
  description: String,
  sections: [{ type: Schema.Types.ObjectId, ref: 'Section' }]
}, {
  collection: 'maps'
});

module.exports = mongoose.model('Map', mapScheme);