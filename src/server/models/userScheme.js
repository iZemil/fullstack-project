import mongoose from 'mongoose';
import bcrypt from 'bcrypt';


const userScheme = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: String,
    role: ['user'],
    createdAt: { type: Date, default: Date.now },
}, {
    collection: 'users'
});


userScheme.methods.comparePassword = function comparePassword(password, callback) {
    bcrypt.compare(password, this.password, callback);
};


/**
 * The pre-save hook method.
 * TODO: Need add profile validation
 */
userScheme.pre('save', function saveHook(next) {
    const user = this;

    // proceed further only if the password is modified or the user is new
    if (!user.isModified('password')) return next();


    return bcrypt.genSalt(function(saltError, salt) {
        if (saltError) return next(saltError);

        return bcrypt.hash(user.password, salt, function(hashError, hash) {
            if (hashError) { return next(hashError); }

            // replace a password string with hash value
            user.password = hash;

            return next();
        });
    });
});

module.exports = mongoose.model('User', userScheme);