

const mongoConnection = {

    init: (mongoose, isLocal) => {

        if(isLocal) {
            const tunnel = require('tunnel-ssh');
            const config = {
                username: process.env.SSH_USER,
                host: process.env.SSH_PORT,
                port: 22,
                dstPort: 27017,
                password: process.env.SSH_PASS
            };

            tunnel(config, function (error, server) {
                if(error) {
                    console.log("SSH connection error: " + error);
                }

                mongoose.connect(process.env.SSH_DB);

                const db = mongoose.connection;

                db.on('error', console.error.bind(console, 'DB connection error:'));
                db.once('open', function() {
                    console.log("DB connection successful");
                });
            });

        } else {
            mongoose.connect('mongodb://localhost:27017/frontgo');
            mongoose.Promise = global.Promise;
            const db = mongoose.connection;

            db.on('error', console.error.bind(console, 'MongoDB connection error:'));
            db.once('open', console.info.bind(console, 'MongoDB connection success!!!'));
        }
    }
}


export default mongoConnection;