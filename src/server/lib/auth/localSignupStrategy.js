import User from '../../models/userScheme';
import { Strategy as PassportLocalStrategy } from 'passport-local';


module.exports = new PassportLocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    session: false,
    passReqToCallback: true
}, function(req, email, password, done) {

    const userData = {
        email: email.trim(),
        password: password.trim(),
        name: email.trim()
    };

    const newUser = new User(userData);
    newUser.save(function(err) {
        if (err) return done(err);

        return done(null);
    });
});