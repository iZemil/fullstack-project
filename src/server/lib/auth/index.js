require('dotenv').config();

import emailVerifyRoute from '../../routes/auth/emailVerifyRoute';
import localAuthRoute from  '../../routes/auth/localAuthRoute';
import socialAuthRoute from '../../routes/auth/socialAuthRoute';
import User from '../../models/userScheme';
import passport from 'passport';
import GoogleStrategy from './googleStrategy';
import GitHubStrategy from './githubStrategy';
import LocalLogin from './localLoginStrategy';
// import LocalSignup from './localSignupStrategy';

passport.serializeUser(function(user, done) {
    done(null, user._id);
});

passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
        if (err || !user) {
            return done(err, null);
        }
        done(null, user);
    });
});


module.exports = function(app) {

    return  {
        init: function() {

            // Local login strategy:
            passport.use('local-login', LocalLogin);


            passport.use(GoogleStrategy);


            passport.use(GitHubStrategy);

            app.use(passport.initialize());
            app.use(passport.session());
        },

        initSmtp: function() {

            // Routes for email verification by local sign up
            app.use('/', emailVerifyRoute);
        },

        registerRoutes: function() {

            // Routes for local login/logout and sign up:
            app.use('/auth', localAuthRoute);

            // Routes for social media nets:
            app.use('/auth', socialAuthRoute);

        }
    };
};