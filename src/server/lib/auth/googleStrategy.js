require('dotenv').config();

import {OAuth2Strategy as GoogleStrategy} from 'passport-google-oauth';
import User from '../../models/userScheme';


export default new GoogleStrategy({
    clientID:     process.env.GOOGLE_ID,
    clientSecret: process.env.GOOGLE_SECRET,
    callbackURL:  process.env.GOOGLE_CB
}, function(token, tokenSecret, profile, done) {
    const authId = 'google:' + profile.id,
        authEmail = profile.emails[0].value;

    User.findOne({ email: authEmail }, function(err, user) {
        if (err) return done(err, null);

        if (user) return done(null, user);

        user = new User({
            authId: authId,
            name: profile.displayName,
            email: profile.emails[0].value,
            createdAt: Date.now()
        });

        user.save(function(err) {
            if (err) return done(err, null);

            done(null, user);
        });
    });
});