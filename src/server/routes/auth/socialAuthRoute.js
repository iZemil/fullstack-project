import express from 'express';
import socialAuth from '../../controllers/auth/socialAuthController';

const socialAuthRoute = express.Router();


// GOOGLE:
socialAuthRoute.get('/google', socialAuth.google);
socialAuthRoute.get('/google/callback', socialAuth.googleCb, socialAuth.googleAfterCb);

// GITHUB:
socialAuthRoute.get('/github', socialAuth.github);
socialAuthRoute.get('/github/callback', socialAuth.githubCb, socialAuth.githubAfterCb);

// FACEBOOK:
socialAuthRoute.get('/facebook', socialAuth.facebook);
socialAuthRoute.get('/facebook/callback', socialAuth.facebookCb, socialAuth.facebookAfterCb);


export default socialAuthRoute;