import express from 'express';
import emailVerifyController from '../../controllers/auth/emailVerifyController';

const emailVerifyRouter = express.Router();


// Verify email
emailVerifyRouter.get('/email-verify', emailVerifyController.verifyData);

// Post data to verify
emailVerifyRouter.post('/email-verify', emailVerifyController.sendData);


export default emailVerifyRouter;