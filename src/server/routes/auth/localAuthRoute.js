import express from 'express';
import localAuth from '../../controllers/auth/localAuthController';

const localAuthRoute = express.Router();


// LOG IN
localAuthRoute.post('/login', localAuth.login);

// LOG OUT
localAuthRoute.get('/logout', localAuth.logout);

// SIGH UP
// localAuthRoute.post('/signup', localAuth.signup);


export default localAuthRoute;