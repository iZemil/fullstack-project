const express = require('express'),
      map = express.Router(),
      mapController = require('../controllers/mapController');


// GET: /:map_id
map.route('/:map_id').get(mapController.map_get);

module.exports = map;