require('dotenv').config();

const express = require('express'),
      user = express.Router(),
      userController = require('../controllers/userController');


// GET
user.route('/').get(userController.user_get);


// POST
user.route('/skills').post(userController.user_post_skills);

// PUT
user.put('/profile', userController.user_put_profile);


module.exports = user;