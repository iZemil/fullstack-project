require('dotenv').config();
const path = require('path'),
    merge = require('webpack-merge'),
    baseConfig = require('./webpack.base'),
    CleanWebpackPlugin  = require('clean-webpack-plugin');


const devExternals = {
    "../node_modules/react/umd/react.development.js": "React",
    "../node_modules/react-dom/umd/react-dom.development.js": "ReactDOM"
};
const prodExternals = {
    "../node_modules/react/umd/react.production.min.js": "React",
    "../node_modules/react-dom/umd/react-dom.production.min.js": "ReactDOM"
};

const config = {

    entry: './src/client/',

    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'public'),
    },

    plugins: [
        new CleanWebpackPlugin(['public'], { root: path.resolve(__dirname , ''), verbose: true}),
    ],

    externals: process.env.IS_LOCAL ? devExternals : prodExternals,

};

module.exports = merge(baseConfig, config);