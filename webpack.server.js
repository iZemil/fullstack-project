const path = require('path'),
    merge = require('webpack-merge'),
    baseConfig = require('./webpack.base.js'),
    webpackNodeExternals = require('webpack-node-externals'),
    CleanWebpackPlugin  = require('clean-webpack-plugin');


const config = {
    target: 'node',

    entry: './src/server/index.js',

    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'build'),
    },

    plugins: [
        new CleanWebpackPlugin(['build'], { root: path.resolve(__dirname , ''), verbose: true}),
    ],

    externals: [ webpackNodeExternals() ]
};


module.exports = merge(baseConfig, config);